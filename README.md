#London Bus Routes#

## How to setup ##


```
#!javascript
npm install -g gulp karma karma-cli webpack
npm install
```

## Running ##
Executing the following will start a dev server, watch:
```
#!javascript
gulp
```

Running tests can be executed with:

```
#!javascript

npm test
```