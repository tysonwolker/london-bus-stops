import template from './home.html';
import controller from './home.controller.js';
import './home.styl';

let homeComponent = function () {
    return {
        restrict: 'E',
        template,
        controller,
        controllerAs: 'vm',
        bindToController: true
    };
};

export default homeComponent;