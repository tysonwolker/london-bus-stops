import angular from 'angular';
import uiRouter from 'angular-ui-router';
import googleMaps from 'angular-google-maps';
import angularSimpleLogger from 'angular-simple-logger';
import lodash from 'lodash';
import mapComponent from './map.component';
let mapModule = angular.module('map', [
    uiRouter,
    'uiGmapgoogle-maps'
])

    .directive('map', mapComponent);

export default mapModule;