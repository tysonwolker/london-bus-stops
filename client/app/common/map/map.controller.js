class MapController {
    constructor(BusStops) {
        let self = this;
        this.map = { center: { latitude: 51.51576931032893, longitude: -0.07215090385917805 }, zoom: 8 };
        this.events = {
            click: function(marker, eventName, busStop) {

            }
        };
        this.options = {
            scrollwheel: false
        };
        this.busStops = [];
        BusStops.getBusStops().then((busStops) => {
            self.busStops = busStops;
        });
    }
}
MapController.$inject = ['BusStops'];
export default MapController;