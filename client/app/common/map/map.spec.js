import MapModule from './map'
import MapController from './map.controller';
import MapComponent from './map.component';
import MapTemplate from './map.html';

describe('Map', () => {
    let $rootScope, makeController, $q;

    beforeEach(window.module(MapModule.name));
    beforeEach(inject((_$rootScope_, _$q_) => {
        $rootScope = _$rootScope_;
        $q = _$q_;
        makeController = (busStopService) => {
            return new MapController(busStopService);
        };
    }));

    describe('Controller', () => {
        var busStopService = jasmine.createSpyObj('BusStop', ['getBusStops']);
       it('gets bus stops from service and attaches to controller on success', () => {
           let busStops = [{
               latitude: 51.01,
               longitude: 0.12
           }];
           busStopService.getBusStops.and.callFake(() => {
               var deferred = $q.defer();
               deferred.resolve(busStops);
               return deferred.promise;
           });

           let controller = makeController(busStopService);
           $rootScope.$digest();

           expect(controller.busStops).toEqual(busStops);
           expect(busStopService.getBusStops).toHaveBeenCalled();
       });
    });

    describe('Component', () => {
        // component/directive specs
        let component = MapComponent();

        it('includes the intended template',() => {
            expect(component.template).toEqual(MapTemplate);
        });

        it('uses `controllerAs` syntax', () => {
            expect(component['controllerAs']).toBeDefined();
        });

        it('invokes the right controller', () => {
            expect(component.controller).toEqual(MapController);
        });
    });
});