import template from './map.html';
import controller from './map.controller';
import './map.styl';

let mapComponent = function () {
    return {
        restrict: 'E',
        scope: {},
        template,
        controller,
        controllerAs: 'vm',
        bindToController: true
    };
};

export default mapComponent;