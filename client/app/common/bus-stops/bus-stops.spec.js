import BusStopsModule from './bus-stops'
import BusStopsService from './bus-stops.service';

describe('Bus Stops', () => {
    let busStopsService, $q, $httpBackend;

    beforeEach(window.module(BusStopsModule.name));
    beforeEach(inject((_BusStops_, _$q_, _$httpBackend_) => {
        busStopsService = _BusStops_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('Service', () => {
        describe('Function: getBusStops', () => {

            it('on success should map bus stop response to google maps api', () => {
                let busStops = [{
                    "id": "50125",
                    "smsCode": "50125",
                    "name": "Aldgate East Station",
                    "stopIndicator": "C",
                    "towards": null,
                    "direction": "se",
                    "lat": 51.51576931032893,
                    "lng": -0.07215090385917805,
                    "routes": [{"id": "67", "name": "67"}]
                }];
                $httpBackend.whenJSONP(/.*/).respond({markers: busStops});
                $httpBackend.expectJSONP('http://digitaslbi-id-test.herokuapp.com/bus-stops?callback=JSON_CALLBACK&northEast=51.52783450,-0.04076115&southWest=51.51560467,-0.10225884');
                let result = undefined;

                busStopsService.getBusStops().then(function (busStops) {
                    result = busStops;
                });
                $httpBackend.flush();

                expect(result).toEqual([Object({
                    id: '50125',
                    latitude: 51.51576931032893,
                    longitude: -0.07215090385917805,
                    smsCode: '50125',
                    title: 'Aldgate East Station',
                    stopIndicator: 'C',
                    towards: null,
                    direction: 'se',
                    routes: [{id: '67', name: '67'}]
                })]);

            });
        });
    });


});