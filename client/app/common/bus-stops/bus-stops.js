import angular from 'angular';
import BusStopsService from './bus-stops.service';

let busStopsModule = angular.module('busStops', [])

    .service('BusStops', BusStopsService);

export default busStopsModule;