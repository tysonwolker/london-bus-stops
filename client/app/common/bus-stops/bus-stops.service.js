import lodash from 'lodash';
class BusStopsService {
    constructor($http) {
        this.$http = $http;
    }
    getBusStops() {
        return this.$http.jsonp('http://digitaslbi-id-test.herokuapp.com/bus-stops?callback=JSON_CALLBACK&northEast=51.52783450,-0.04076115&southWest=51.51560467,-0.10225884')
                .then(r => {
                var busStops = lodash.map(r.data.markers, (marker) => {
                    return {
                        id: marker.id,
                        latitude: marker.lat,
                        longitude: marker.lng,
                        smsCode: marker.smsCode,
                        title: marker.name,
                        stopIndicator: marker.stopIndicator,
                        towards: marker.towards,
                        direction: marker.direction,
                        routes: marker.routes
                    };
                });
                return busStops;
            });
    }
}
BusStopsService.$inject = ['$http'];
export default BusStopsService;