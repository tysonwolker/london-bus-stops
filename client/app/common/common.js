import angular from 'angular';
import Map from './map/map';
import BusStops from './bus-stops/bus-stops';

let commonModule = angular.module('app.common', [
    Map.name,
    BusStops.name
]);

export default commonModule;